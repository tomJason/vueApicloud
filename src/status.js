import Vue from 'vue';
import vTap from 'v-tap';
export function status (App){
	// const sta = 'api';
	const sta = 'pc';
	if(sta=='pc'){
	  new Vue({
	      el: '#app',
	      template:'<App/>',
	      components:{
	        App
	      }
	  })
	}else if(sta=='api'){
	  window.apiready = function() {
	      new Vue({
	          el: '#app',
	          template:'<App/>',
	          components:{
	          	App
	          }
	      })
	  }
	}
	Vue.use(vTap); //添加vue tap事件
}
